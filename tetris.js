const WIDTH = 1200;
const HEIGHT = 900;
const ROW = 10;
const COL = 20;
const SIZE = 30;
const BOARD_WIDTH = SIZE*ROW;
const BOARD_HEIGHT = SIZE*COL;
const ORIGIN = {
  x: (WIDTH - BOARD_WIDTH)/2,
  y: (HEIGHT - BOARD_HEIGHT)/2
}
const BOARD_COLOR = "black"

let targetNode = document.querySelector("main");
let canvas = document.createElement("canvas");
let event;
let screenSurface;
let frameCount = 0;

function initScreen() {
  // Append canvas to targetNode
  targetNode.appendChild(canvas);
  // Get screenSurface
  screenSurface = canvas.getContext("2d");
  // Setup Screen
  setup();
}

function initEvent() {
  window.addEventListener('keydown', onKeyDown);
  window.addEventListener('keyup', onKeyUp);
}

function createCanvas(width = 20, height = 20) {
  if (!canvas) return;
  canvas.setAttribute("width", width);
  canvas.setAttribute("height", height);
}

function background(color) {
  if (!screenSurface) return;
  screenSurface.fillStyle = color;
  screenSurface.fillRect(0, 0, WIDTH, HEIGHT);
}

function font(str) {
  if (!screenSurface) return;
  screenSurface.font = str;
}

function textAlign(str) {
  if (!screenSurface) return;
  screenSurface.textAlign = str;
}

function text(str, x, y, maxWidth) {
  if (!screenSurface) return 0;
  screenSurface.fillText(str, x, y, maxWidth);
}

function rect(x, y, w, h) {
  if (!screenSurface) return;
  screenSurface.fillRect(x, y, w, h);
}

function quad(x1, y1, x2, y2, x3, y3, x4, y4) {
  if (!screenSurface) return;
  screenSurface.beginPath();
  screenSurface.moveTo(x1, y1);
  screenSurface.lineTo(x2, y2);
  screenSurface.lineTo(x3, y3);
  screenSurface.lineTo(x4, y4);
  screenSurface.fill();
}

function color(str) {
  if (!screenSurface) return;
  screenSurface.fillStyle = str;
}

function refresh(x, y, w, h) {
  if (!screenSurface) return;
  screenSurface.clearRect(x, y, w, h);
}

let dropping = false;
let appearanceDelay = 0;
let currentTetrominoe;
let nextTetrominoe;
let nextId;
let tetrominoes = new Array(COL).fill().map(() => new Array(ROW).fill());
let count = new Array(7).fill(0);
let score = 0;
let level = 0;
let lines = 0;
let pieceHistory = 0;
let softDrop = false;
let gameover = false;
let startMenu = true;

function rand() {
  let firstRoll = Math.floor(Math.random() * (freeTetrominoes.length + 1));
  let secondRoll;
  if (firstRoll === pieceHistory || firstRoll === 7) {
    secondRoll = Math.floor(Math.random() * (freeTetrominoes.length));
  }
  pieceHistory = secondRoll || firstRoll;
  return pieceHistory;
}

function initGame() {
  dropping = true;
  appearanceDelay = 0;
  const random = rand();
  const randomNext = rand();
  currentTetrominoe = new Tetrominoe(freeTetrominoes[random]);
  nextTetrominoe = new Tetrominoe(freeTetrominoes[randomNext]);
  nextId = randomNext;
  count[random] += 1;
  tetrominoes = new Array(COL).fill().map(() => new Array(ROW).fill());
  score = 0;
  level = 0;
  lines = 0;
  softDrop = false;
  gameover = false;
  startMenu = false;
}

function pushToTetrominoes(t) {
  for (let i = 0; i < t.shape.length; i++) {
    const [x, y] = t.shape[i];

    if (x < 0 || x > ROW - 1 || y < 0 || y > COL - 1) continue;
    
    tetrominoes[y][x] = t.color;
  }
}

function onKeyDown(e) {
  if (e.key === "x" && (startMenu || gameover)) {
    initGame();
    return;
  }

  if (!currentTetrominoe) return;
  
  if (e.key === "ArrowLeft" && dropping) {
    currentTetrominoe.left();
  } else if (e.key === "ArrowRight" && dropping) {
    currentTetrominoe.right();
  } else if (e.key === "ArrowDown" && dropping) {
    currentTetrominoe.softDrop();
  } else if (e.key === "z" && dropping) {
    // counterclockwise rotate
    currentTetrominoe.counterClockwiseRotate();
  } else if (e.key === "x" && dropping) {
    // clockwise rotate
    currentTetrominoe.clockwiseRotate();
  }
}

function onKeyUp(e) {
  if (e.key === "ArrowDown") {
    softDrop = false;
  }
}

function step() {  
  if (screenSurface) {
    background("black");

    // draw board
    drawBoard();

    if (startMenu) {
      drawStartMenu();
    } else if (gameover) {
      drawGameOver();
    } else if (appearanceDelay > 0) {
      appearanceDelay -= 1;
    } else if (dropping) {
      const fpg = softDrop ? 1 : levelToFPG(level);
      if (frameCount % fpg === 0) {
        currentTetrominoe.drop();
      }
    } else {
      if (checkOverlap(currentTetrominoe.shape)) {
        gameover = true;
      } else {
        // push current tetrominoe to tetrominoes
        pushToTetrominoes(currentTetrominoe);
        // check fulllines and calculate score
        const removedLines = removeLines();
        score += calcScore(removedLines);
        // append lines and calculate level
        lines += removedLines;
        level = Math.floor(lines / 10);
        // calculate ARE
        const maxY = Math.max(...currentTetrominoe.shape.map(([x, y]) => y));
        // 1. 10~18 frames depending on the height at which the piece locked
        appearanceDelay = 10 + Math.floor(maxY / 4) * 2;
        // 2. line clear delay is an additional 17~20 frames depending on the frame that the piece locks
        if (removedLines > 0) {
          appearanceDelay = appearanceDelay + 17 + levelToFPG(level) % 4;
        }
        // disable softDrop
        softDrop = false;
        // new currentTetrominoe and nextTetrominoe
        currentTetrominoe = nextTetrominoe;
        count[nextId] += 1;
        const random = rand();
        nextTetrominoe = new Tetrominoe(freeTetrominoes[random]);
        nextId = random;
        // enable dropping
        dropping = true;
      }
    }

    if (currentTetrominoe && appearanceDelay === 0 && !gameover && !startMenu)
      drawTetrominoe(currentTetrominoe)
    if (nextTetrominoe)
      drawTetrominoe(nextTetrominoe, ORIGIN.x + SIZE*9, ORIGIN.y + SIZE*15);

    if (!gameover && !startMenu)
      drawTetrominoes();

    drawCount();
    drawStats();

    drawGuide();
    
    frameCount++;
  }

  window.requestAnimationFrame(step);
}

function setup() {
  createCanvas(WIDTH, HEIGHT);
}

const Tetrominoe = function(s) {
  const { name, color, shape } = s;
  this.name = name;
  this.color = color;
  this.shape = JSON.parse(JSON.stringify(shape));
}

Tetrominoe.prototype.softDrop = function() {
  softDrop = true;
  this.drop();
}

Tetrominoe.prototype.checkAndUpdateShape = function(fn) {
  const newShape = this.shape.map(fn);
  if (!checkOverlap(newShape) && !checkRange(newShape)) this.shape = newShape;
}

Tetrominoe.prototype.checkAndRotateShape = function(clockwise) {
  if (this.name === "O") return;
  
  const [base, _] = this.shape;
  const [baseX, baseY] = base;

  if (clockwise) {
    this.checkAndUpdateShape(([x, y]) => [baseX + (baseY - y), baseY + (x - baseX)]);
  } else {
    this.checkAndUpdateShape(([x, y]) => [baseX - (baseY - y), baseY - (x - baseX)]);
  }
}

Tetrominoe.prototype.drop = function() {
  const newShape = this.shape.map(([x, y]) => [x, y+1]);

  if (checkOverlap(newShape)) {
    dropping = false;
  } else if (!checkRange(newShape)) {
    this.shape = newShape;
  }
};

Tetrominoe.prototype.left = function() { this.checkAndUpdateShape(([x, y]) => [x-1, y]); };

Tetrominoe.prototype.right = function() { this.checkAndUpdateShape(([x, y]) => [x+1, y]); };

Tetrominoe.prototype.clockwiseRotate = function() { this.checkAndRotateShape(true); };

Tetrominoe.prototype.counterClockwiseRotate = function() { this.checkAndRotateShape(false); };

function checkRange(newShape) {
  for (let i = 0; i < newShape.length; i++) {
    const [x, y] = newShape[i];
    
    if (y < 0) continue;

    if (x < 0 || x > ROW - 1) {
      return true;
    }
  }
  return false;
}

function checkOverlap(newShape) {
  for (let i = 0; i< newShape.length; i++) {
    const [x, y] = newShape[i];

    if (y < 0) continue;
    
    // bottom | left | right | overlap with tetrominoes
    if (y > COL - 1 || tetrominoes[y][x]) {
      return true;
    }
  }
  return false;
}

function removeLines() {
  // find the lines
  const fullLineNumbers = tetrominoes.reduce((acc, line, index) => {
    const isFull = line.reduce((acc, cur) => cur ? acc + 1 : acc, 0) === ROW;
    return isFull ? acc.concat(index) : acc;
  }, []);

  // remove lines
  if (fullLineNumbers.length <= 0) return 0;
  
  for (let i = 0; i < fullLineNumbers.length; i++) {
    tetrominoes.splice(fullLineNumbers[i], 1);
    tetrominoes.splice(0, 0, new Array(ROW).fill());
  }

  return fullLineNumbers.length;
}

function calcScore(lines) {
  if (lines >= 4) {
    return 1200 * (level + 1) + calcScore(lines - 4) + (softDrop ? 4 : 0);
  } else if (lines >= 3) {
    return 300 * (level + 1) + calcScore(lines - 3) + (softDrop ? 3 : 0);
  } else if (lines >= 2) {
    return 100 * (level + 1) + calcScore(lines - 2) + (softDrop ? 2 : 0);
  } else if (lines === 1) {
    return 40 * (level + 1) + calcScore(lines - 1) + (softDrop ? 1 : 0);
  } else {
    return 0;
  }
}

function levelToFPG(level) {
  if (level >= 0 && level <= 8) {
    return 48 - level * 5;    
  } else if (level === 9) {
    return 6;
  } else if (level >= 10 && level <= 12) {
    return 5;
  } else if (level >= 13 && level <= 15) {
    return 4;
  } else if (level >= 16 && level <= 18) {
    return 3;
  } else if (level >= 19 && level <= 28) {
    return 2;
  } else if (level >= 29) {
    return 1;
  } else {
    return 0;
  }
}

const I = {
  name: "I",
  color: "rgb(0, 240, 240)",
  shape: [
    [5, 0], [3, 0], [4, 0], [6, 0]
  ]
}

const O = {
  name: "O",
  color: "rgb(240, 240, 0)",
  shape: [
    [4, 0], [5, 0],
    [4, 1], [5, 1]
  ]
};

const T = {
  name: "T",
  color: "rgb(162, 0, 240)",
  shape: [
    [5, 0], [4, 0], [6, 0],
    [5, 1]
  ]
};

const J = {
  name: "J",
  color: "rgb(0, 0, 240)",
  shape: [
    [5, 0], [4, 0], [6, 0],
    [6, 1]
  ]
};

const L = {
  name: "L",
  color: "rgb(240, 161, 0)",
  shape: [
    [5, 0], [4, 0], [6, 0],
    [4, 1]
  ]
};

const S = {
  name: "S",
  color: "rgb(0, 240, 0)",
  shape: [
    [5, 0], [6, 0],
    [4, 1], [5, 1]
  ]
};

const Z = {
  name: "Z",
  color: "rgb(240, 0, 0)",
  shape: [
    [5, 0], [4, 0],
    [5, 1], [6, 1]
  ]
};

const freeTetrominoes = [I, O, T, L, J, S, Z];

function drawBlock(x, y, c, size,
                   sw = 3,
                   tc = "rgba(255, 255, 255, .7)",
                   sc = "rgba(0, 0, 0, .1)",
                   bc = "rgba(0, 0, 0, .5)") {
  // square
  color(c);
  rect(x, y, size, size);
  
  // top
  color(tc);
  quad(x, y, x + size, y, x + size - sw, y + sw, x + sw, y + sw);
  
  // left and right
  color(sc);
  quad(x, y, x + sw, y + sw, x + sw, y + size - sw, x, y + size);
  quad(x + size, y, x + size - sw, y + sw, x + size - sw, y + size - sw, x + size, y + size);
  
  // bottom
  color(bc);
  quad(x, y + size, x + size, y + size, x + size - sw, y + size - sw, x + sw, y + size - sw);
}

function drawTetrominoe(t, x, y, s) {
  const {shape, color: c} = t;
  const baseX = (x || ORIGIN.x);
  const baseY = (y || ORIGIN.y);
  const size = s || SIZE;

  for (let i = 0; i < shape.length; i++) {
    if (shape[i][1] < 0) continue;

    const x = baseX + shape[i][0] * size;
    const y = baseY + shape[i][1] * size;
    
    drawBlock(x, y, c, size);
  }
}

function drawBoard() {  
  // background
  color(BOARD_COLOR);
  rect(ORIGIN.x, ORIGIN.y, BOARD_WIDTH, BOARD_HEIGHT); 

  // border
  for (let i = 0; i < ROW; i++) {
    drawBlock(ORIGIN.x+i*SIZE, ORIGIN.y - SIZE, c = "rgb(120, 120, 120)", SIZE, sw = 2);
    drawBlock(ORIGIN.x+i*SIZE, ORIGIN.y+COL*SIZE, c = "rgb(120, 120, 120)", SIZE, sw = 2);
  }
  for (let i = -1; i <= COL; i++) {
    drawBlock(ORIGIN.x - SIZE, ORIGIN.y+i*SIZE, c = "rgb(120, 120, 120)", SIZE, sw = 2);
    drawBlock(ORIGIN.x+ROW*SIZE, ORIGIN.y+i*SIZE, c = "rgb(120, 120, 120)", SIZE, sw = 2);
  }
}

function drawTetrominoes() {
  // static tetrominoes
  for (let i = 0; i < tetrominoes.length; i++) {
    for (let j = 0; j < tetrominoes[i].length; j++) {
      if (tetrominoes[i][j]) {
        drawBlock(ORIGIN.x+j*SIZE, ORIGIN.y+i*SIZE, tetrominoes[i][j], SIZE);
      }
    }
  }
}

function drawStartMenu() {
  color(`rgba(240, 240, 240, 1)`);
  font(`${SIZE * 0.8}px san-serif`);
  textAlign("center");
  text("PRESS X TO START", WIDTH / 2, HEIGHT / 2);
}

function drawGameOver() {
  rect(ORIGIN.x, ORIGIN.y + 10*SIZE, SIZE, SIZE);
  color(`rgba(240, 240, 240, 1)`);
  font(`${SIZE * 0.8}px san-serif`);
  textAlign("center")
  text("GAME OVER", WIDTH / 2, HEIGHT / 2);
  font(`${SIZE * 0.6}px san-serif`);
  text("PRESS X TO RESTART", WIDTH / 2, (HEIGHT / 2) + SIZE);
}

function drawCount() {
  for (let i = 0; i < freeTetrominoes.length; i++) {
    drawTetrominoe(freeTetrominoes[i], ORIGIN.x - SIZE * 9, ORIGIN.y + SIZE * i * 2 + SIZE, SIZE * 0.7);
    color("white");
    font(`${SIZE}px san-serif`);
    textAlign("right");
    text(count[i], ORIGIN.x - SIZE * 2, ORIGIN.y + SIZE * i * 2 + SIZE + SIZE);
  }
}

function drawStats() {
  const x = ORIGIN.x + BOARD_WIDTH + SIZE * 2;
  const y = ORIGIN.y + SIZE;
  color("white");
  font(`${SIZE}px san-serif`);
  textAlign("left");
  text("SCORE: ", x, y + SIZE);
  text(score, x, y + SIZE * 2);
  text("LEVEL: ", x, y + SIZE * 4);
  text(level, x, y + SIZE * 5);
  text("LINES: ", x, y + SIZE * 7);
  text(lines, x, y + SIZE * 8);
  text("NEXT: ", x, y + SIZE * 12);
}

function drawGuide() {
  const x = ORIGIN.x - SIZE * 2;
  const y = ORIGIN.y + SIZE;
  const lst = ["move left:  ←", "move right:  →", "soft drop:    ↓",
               "rotate left:    x", "rotate right:    z"];
  color("white");
  font(`${SIZE * 0.8}px san-serif`);
  textAlign("right");
  for (let i = 0; i < lst.length; i++) {
    text(lst[i], x, y + SIZE * (15+i));
  }
}

initScreen();
initEvent();
step();
